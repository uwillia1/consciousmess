class Ripple < ActiveRecord::Base

	validates :name, :message, presence: true
	
	before_destroy :prevent_destroy
	
	def prevent_destroy
		false
	end
	
	before_update :prevent_update
	
	def prevent_update
		false
	end

end
