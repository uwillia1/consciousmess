json.array!(@ripples) do |ripple|
  json.extract! ripple, :id, :name, :url, :message
  json.url ripple_url(ripple, format: :json)
end
